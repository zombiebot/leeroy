(ns leeroy.utils
  (:gen-class)
  (:require [clj-time.coerce :as c]
            [clj-time.local :as l]
            [clj-time.format :as f]
            [clj-http.client :as client]
            [me.raynes.fs :as fs]
            [clojure.tools.logging :as log]))

(def not-nil? (complement nil?))

(defn ^String substring?
  "True if s contains the substring."
  [substring ^String s]
  (if (nil? s)
    false
    (.contains s substring)))

(defn save-report
  "save report result to html file"
  [result]
  (let [filename-formatter (f/formatter-local "yyyy-MM-dd hh-mm-ss")
        file-name (str (f/unparse filename-formatter (l/local-now)) ".html")
        folder-name "Reports"]
    (fs/mkdir folder-name)
    (spit (str folder-name "/" file-name) result)))

(defn get-page
  "returns page content or nil (if exception occurs)"
  [url]
  (try
    (client/get url)
    (catch Exception e
      (log/info
       (str "There was an error while getting page content from url: '" url "' - ")
       (.getMessage e)))))
